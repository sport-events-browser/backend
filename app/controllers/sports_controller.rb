class SportsController < ApplicationController
  require 'json'
  require 'open-uri'

  before_action :set_sport, only: %i[ show ]

  # GET /sports
  def index
    @sports = get_json['sports']

    @response = []

    @sports.each do |sport|
      data = {"id" => sport['id'], "desc" => sport['desc']}
      @response.push(data)
    end
    if !@response.empty?
      render json: @response
    else
      render :status => 404, json: {message: '404 unable to fetch sports'}
    end
    
  end

  # GET /sports/1
  def show
    @events = []

    @sport['comp'].each do |comp|
      comp['events'].each do |event|
        data = {"id" => event['id'], "desc" => event['desc']}
        @events.push(data)
      end
    end

    @response = {"id" => @sport['id'], "desc" => @sport['desc'], "events" => @events}

    render json: @response
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sport
      @sports = get_json['sports']

      @sports.each do |sport|
        if sport['id'] == params[:id].to_i
          @sport = sport
          return
        end
      end

      render :status => 404, json: {message: '404 sport not found'}
    end

    # Only allow a list of trusted parameters through.
    def sport_params
      params.require(:sport).permit(:id)
    end
end
