require "test_helper"

class SportsControllerTest < ActionDispatch::IntegrationTest

  teardown do
    Rails.cache.clear
  end

  test "should get index" do
    get sports_url, as: :json
    assert_response :success
  end

  test "should show sport" do
    get sport_url(100), as: :json
    assert_response :success
  end
end
