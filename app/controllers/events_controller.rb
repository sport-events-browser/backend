class EventsController < ApplicationController
  before_action :set_event, only: %i[ show ]

  # GET /sports/:id/events/:id
  def show
    @response = []

    if !@event
      render :status => 404, json: {message: '404 event not found'}
    else
    
      @event['markets'][0]['o'].each do |outcome|
        data = {"id" => outcome['oid'], "desc" => outcome['d'], "odds" => outcome['pr']}
        @response.push(data)
      end

      render json: @response
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @sports = get_json['sports']

      @sports.each do |sport|
        if sport['id'] == params[:sport_id].to_i
          sport['comp'].each do |comp|
            comp['events'].each do |event|
              if event['id'] == params[:id].to_i
                @event = event
                return
              end
            end
          end
          return
        end
      end
      render :status => 404, json: {message: '404 sport not found'}
    end

    # Only allow a list of trusted parameters through.
    def event_params
      params.require(:event).permit(:id)
    end
end
