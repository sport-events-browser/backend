Rails.application.routes.draw do
  resources :sports, only: [:index, :show] do
    resources :events, only: [:show]
  end
  # Define your application routes per the DSL in https://guides.rubyonrails.org/routing.html

  # Defines the root path route ("/")
  # root "articles#index"
end
