require 'open-uri'
class ApplicationController < ActionController::API
  def get_json
    @data = Rails.cache.fetch('sports', :expires_in => 30.second) do
      puts "updating cache"
      source_url = "https://www.betvictor.com/bv_in_play/v2/en-gb/1/mini_inplay.json"
      JSON.load(URI.open(source_url))
    end
  end
end
